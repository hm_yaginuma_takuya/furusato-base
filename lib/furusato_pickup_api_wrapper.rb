require 'net/http'
require 'json'

# furusato-pickup-apiを扱うラッパーモジュール
module FurusatoPickupApiWrapper
  ENDPOINT_ORDERS                      = 'orders'
  ENDPOINT_PICKUP_REQUEST_QUES         = 'pickup_request_ques'
  ENDPOINT_MAIL_SEND_TO_SUPPLIER_QUES  = 'mail_send_to_supplier_ques'
  ENDPOINT_PICKUP_CONFIRMATION_LOGS    = 'pickup_confirmation_logs'
  ENDPOINT_PICKUP_SUPPLIERS            = 'suppliers'
  ENDPOINT_PICKUP_REQUEST              = 'pickup_request'

  METHOD_GET                           = 'Get'
  METHOD_POST                          = 'Post'

  PARAMS_RESPONSE_DATA                 = :data

  module_function

  class << self

    def orders(company_id)
      execute(ENDPOINT_ORDERS, METHOD_GET, {company_id: company_id})
    end

    def pickup_request_ques(company_id)
      execute(ENDPOINT_PICKUP_REQUEST_QUES, METHOD_GET, {company_id: company_id})
    end

    def mail_send_to_supplier_ques(company_id)
      execute(ENDPOINT_MAIL_SEND_TO_SUPPLIER_QUES, METHOD_GET, {company_id: company_id})
    end

    def pickup_confirmation_logs
      execute(ENDPOINT_PICKUP_CONFIRMATION_LOGS, METHOD_GET)
    end

    def suppliers(company_id)
      execute(ENDPOINT_PICKUP_SUPPLIERS, METHOD_GET, {company_id: company_id})
    end

    def pickup_request(company_id, supplier, receive_order_ids)
      execute(
        ENDPOINT_PICKUP_REQUEST,
        METHOD_POST,
        {
          company_id: company_id,
          supplier: supplier,
          receive_order_ids: receive_order_ids
        }
      )
    end

    private

    def execute(endpoint, method, params = nil)
      params  = URI.encode_www_form(params) unless params.nil?
      uri     = URI.parse("#{domain}/api/#{version}/#{endpoint}?#{params}")
      request = "Net::HTTP::#{method}".constantize.new(uri)

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end

      JSON.parse(
        response.body,
        { symbolize_names: true }
      )[PARAMS_RESPONSE_DATA]
    end

    def req_options
      {
        use_ssl:     true,
        verify_mode: OpenSSL::SSL::VERIFY_NONE,
      }
    end

    def domain
      ENV['FURUSATO_PICKUP_API_DOMAIN']
    end

    def version
      ENV['FURUSATO_PICKUP_API_VERSION']
    end
  end
end

