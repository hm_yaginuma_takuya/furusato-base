namespace :dynamodb do
  desc "dynamoDBにセッションストアで利用するテーブルを作成する"
  task create_table: :environment do
    dynamo = Aws::DynamoDB::Client.new(
      endpoint: endpoint,
        region: region,
        credentials: credentials
    )

    options = {
        table_name: "sessions",
        attribute_definitions: [
            {
                attribute_name: "session_id",
                attribute_type: "S"
            }
        ],
        key_schema: [
            {
                attribute_name: "session_id",
                key_type: "HASH"
            }
        ],
        provisioned_throughput: {
            read_capacity_units:  10,
            write_capacity_units:  5
        }
    }
    dynamo.create_table(options)
    # dynamo.scan(table_name: 'sessions')
    # p dynamo.describe_table({table_name: "sessions"})
  end

  def credentials
    Aws::Credentials.new(
      access_key_id,
      secret_access_key
    )
  end

  def access_key_id
    ENV["AWS_ACCESS_KEY_ID"]
  end

  def secret_access_key
    ENV["AWS_SECRET_ACCESS_KEY"]
  end

  def endpoint
    ENV['AWS_ENDPOINT']
  end

  def region
    ENV['AWS_REGION']
  end
end
