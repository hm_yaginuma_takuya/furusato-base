require 'net/http'
require 'json'

# FurusatoBaseAPIを扱うラッパーモジュール
module FurusatoBaseApiWrapper
  FURUSATO_BASE_API_DOMAIN_DEFAULT     = 'https://furusato-base-api:3000'
  FURUSATO_BASE_API_VERSION_DEFAULT    = 'v1'
  ENDPOINT_AUTH                        = 'auth'
  ENDPOINT_USERS                       = 'users'
  ENDPOINT_EXECUTE                     = 'execute'

  METHOD_GET                           = 'Get'
  METHOD_POST                          = 'Post'
  # NEAPIで利用するエンドポイントのパス
  PATH_API_LOGIN_COMPANY               = 'login_company_info'
  PATH_API_LOGIN_USER                  = 'login_user_info'
  PATH_API_SHOP_SEARCH                 = 'master_shop_search'
  PATH_API_ORDER_SEARCH                = 'receiveorder_base_search'
  PATH_API_ORDER_DETAIL_SEARCH         = 'receiveorder_row_search'
  PATH_API_SUPPLIER_SEARCH             = 'master_supplier_search'
  PATH_API_ORDER_RECEIPTED             = 'receiveorder_base_receipted'
  PATH_API_ORDER_SHIPPED               = 'receiveorder_base_shipped'
  PATH_API_ORDER_BULKUPDATE            = 'receiveorder_base_bulkupdate'

  PARAMS_RESPONSE_SUCCESS              = :success
  PARAMS_RESPONSE_RESULT               = :result
  PARAMS_RESPONSE_ACCESS_TOKEN         = :access_token
  PARAMS_RESPONCE_REFRESH_TOKEN        = :refresh_token
  PARAMS_RESPONSE_DATA                 = :data
  PARAMS_RESPONSE_COUNT                = :count

  module_function

  class << self

    def auth(params)
      execute(ENDPOINT_AUTH, METHOD_POST, params)
    end

    def users
      execute(ENDPOINT_USERS, METHOD_GET)
    end

    def api_execute_with_params(user, endpoint, params)
      params = {
        user_id: user[:id],
        endpoint: endpoint,
        params: params
      }
      execute(ENDPOINT_EXECUTE, METHOD_POST, params)
    end

    private

    def execute(endpoint, method, params = nil)
      params  = URI.encode_www_form(params) unless params.nil?
      uri     = URI.parse("#{domain}/api/#{version}/#{endpoint}?#{params}")
      request = "Net::HTTP::#{method}".constantize.new(uri)

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end

      JSON.parse(
        response.body,
        { symbolize_names: true }
      )[PARAMS_RESPONSE_DATA]
    end

    def req_options
      {
        use_ssl:     true,
        verify_mode: OpenSSL::SSL::VERIFY_NONE,
      }
    end

    def domain
      ENV['FURUSATO_BASE_API_DOMAIN'] || FURUSATO_BASE_API_DOMAIN_DEFAULT
    end

    def version
      ENV['FURUSATO_BASE_API_VERSION'] || FURUSATO_BASE_API_VERSION_DEFAULT
    end
  end
end

