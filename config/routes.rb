Rails.application.routes.draw do
  root to: 'pickups#orders'

  # callback
  get 'callback', to: 'oauth#callback'

  get 'pickup/orders', to: 'pickups#orders'
  get 'pickup/pickup_request_ques', to: 'pickups#pickup_request_ques'
  get 'pickup/mail_send_to_supplier_ques', to: 'pickups#mail_send_to_supplier_ques'
  get 'pickup/pickup_confirmation_logs', to: 'pickups#pickup_confirmation_logs'
  get 'pickup/suppliers', to: 'pickups#suppliers'
  post 'pickup/pickup_request', to: 'pickups#pickup_request'
end
