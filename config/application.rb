require_relative 'boot'

%w(
  active_job/railtie
  active_model/railtie
  action_controller/railtie
  action_mailer/railtie
  action_view/railtie
  action_cable/engine
  sprockets/railtie
  rails/test_unit/railtie
).each do |framework|
  begin
    require "#{framework}"
  rescue LoadError
  end
end

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    #

    # Time zone
    config.time_zone = 'Tokyo'

    config.paths.add 'lib', eager_load: true

    config.lograge.enabled = true
    config.lograge.formatter = Lograge::Formatters::Json.new
    config.lograge.base_controller_class = ['ActionController::API', 'ActionController::Base']
    config.lograge.custom_options = lambda do |event|
      {
        exception: event.payload[:exception], # ["ExceptionClass", "the message"]
        exception_object: event.payload[:exception_object] # the exception instance
      }
    end

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        # 許可するドメイン
        origins "furusato-pickup:4000"
        # 許可するヘッダとメソッドの種類
        resource "*",
          headers: :any,
          methods: [:get, :post, :patch, :delete, :head, :options]
      end
    end
  end
end
