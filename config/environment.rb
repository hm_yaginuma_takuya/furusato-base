# Load the Rails application.
require_relative 'application'

class Formatter < Logger::Formatter
  def call(severity, timestamp, progname, msg)
    j = ::JSON.parse(msg)
    temp = {
      level: severity,
      timestamp: format_datetime(timestamp)
      # environment: ::Rails.env
    }.merge(j)
    ::JSON.dump(temp) + "\n"
  rescue JSON::ParserError
    "{\"level\":\"#{severity}\",\"timestamp\":\"#{format_datetime(timestamp)}\",\"message\":\"#{msg}\",\"environment\":\"#{::Rails.env}\"}\n"
  end
end

Rails.application.configure do
  config.log_formatter = Formatter.new
  config.log_tags = [ :uuid ]

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end
end

# Initialize the Rails application.
Rails.application.initialize!
