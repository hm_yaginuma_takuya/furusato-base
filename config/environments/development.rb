Rails.application.configure do
  config.cache_classes = true
  config.paths.add 'lib', eager_load: true
  config.eager_load = true
  config.consider_all_requests_local = true

  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.perform_caching = false
  config.active_support.deprecation = :log
  config.assets.debug = true
  config.assets.quiet = true
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  config.force_ssl = true

  BetterErrors::Middleware.allow_ip! "0.0.0.0/0"

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { address: 'mail_box', port: 1025 }
end
