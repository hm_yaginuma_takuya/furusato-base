module PickupsHelper
  def que_status(order_que)
    status = order_que.nil? ? nil : order_que[:status]

    case status
    when nil then
      return '処理待ち'
    when 100 then
      return '処理中'
    when 200 then
      return '異常終了'
    when 300 then
      return '正常終了'
    end
  end

  def order_status(order)
    status = order[:status]

    case status
    when 20 then
      return '納品書印刷待ち'
    when 40 then
      return '納品書印刷済み'
    when 45 then
      return '配送番号反映済み'
    when 50 then
      return '出荷確定済み'
    end
  end

  def pickup_confirmation_log_status(pickup_confirmation_log)
    if pickup_confirmation_log.nil?
      return '処理待ち'
    else
      return '正常終了'
    end
  end

  def sagawa_download_link(pickup_request_link)
    return '' if pickup_request_link.blank?

    tag.button 'ダウンロード',
      onclick: "location.href='#{pickup_request_link}'",
      class: 'btn btn-sm btn-outline-secondary'
  end

  def mail_download_link(mail_send_to_supplier_link)
    return '' if mail_send_to_supplier_link.blank?

    tag.button 'ダウンロード',
      onclick: "location.href='#{mail_send_to_supplier_link}'",
      class: 'btn btn-sm btn-outline-secondary'
  end
end
