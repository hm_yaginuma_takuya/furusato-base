class PickupsController < ApplicationController
  before_action :set_suppliers, only: [:orders, :suppliers]

  PER = 20

  def orders
    @orders = Kaminari.paginate_array(
      FurusatoPickupApiWrapper.
      orders(current_company[:id])
    ).page(params[:page]).per(PER)
  end

  def pickup_request_ques
    @ques = Kaminari.paginate_array(
      FurusatoPickupApiWrapper.
      pickup_request_ques(current_company[:id])
    ).page(params[:page]).per(PER)
  end

  def mail_send_to_supplier_ques
    @ques = Kaminari.paginate_array(
      FurusatoPickupApiWrapper.
      mail_send_to_supplier_ques(current_company[:id])
    ).page(params[:page]).per(PER)
  end

  def pickup_confirmation_logs
    @pickup_confirmation_logs = Kaminari.paginate_array(
      FurusatoPickupApiWrapper.
      pickup_confirmation_logs
    ).page(params[:page]).per(PER)
  end

  def suppliers
  end

  def pickup_request
    if params[:supplier].blank? && params[:receive_order_id].blank?
      redirect_to pickup_orders_path, danger: '事業者情報または受注伝票番号を入力してください'
    else
      FurusatoPickupApiWrapper.pickup_request(
        current_company[:id],
        params[:supplier],
        params[:receive_order_id]
      )

      redirect_to pickup_orders_path, success: '集荷依頼を受け付けました'
    end
  end

  private

  def set_suppliers
    @suppliers = Kaminari.paginate_array(
      FurusatoPickupApiWrapper.
      suppliers(current_company[:id])
    ).page(params[:page]).per(PER)

    suppliers = FurusatoPickupApiWrapper.
      suppliers(current_company[:id])
    @select_suppliers = {}
    suppliers.map { |supplier|
      @select_suppliers[supplier[:info][:supplier_name]] = supplier[:id]
    }
  end
end
