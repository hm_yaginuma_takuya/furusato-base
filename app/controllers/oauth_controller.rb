class OauthController < ApplicationController
  def callback
    response = FurusatoBaseApiWrapper.auth({
      uid:   params[:uid],
      state: params[:state]
    })

    session[:company] = response[:company]
    session[:user]    = response[:user]

    redirect_to root_path
  end
end
