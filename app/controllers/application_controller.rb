class ApplicationController < ActionController::Base

  helper_method :current_user, :current_company

  add_flash_types :success, :info, :warning, :danger

  def current_user
    session[:user].try(:symbolize_keys)
  end

  def current_company
    session[:company].try(:symbolize_keys)
  end
end
