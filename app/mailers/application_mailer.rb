class ApplicationMailer < ActionMailer::Base
  default from: 'furusato@hamee.co.jp'
  layout 'mailer'
end
