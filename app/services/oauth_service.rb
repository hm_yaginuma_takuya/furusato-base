module OauthService
  # Oauth認証のレスポンスからcompanyとuserオブジェクト生成する
  # @param [Hash] auth_response Oauth認証からのレスポンス
  # @return [Hash] company_idとuser_idのhash { company_id => user_id }
  def register(auth_response)
    begin
      company_info = AuthClient.get_company_information(auth_response)
      company      = Company.find_by(main_function_id_hash: company_info['company_id'])
      company      = AuthClient.company_filter(company, company_info)

      {
        company.id => AuthClient.user_filter(
          company,
          AuthClient.get_user_information(auth_response)
        ).id
      }
    rescue => e
      AppLogger.error("認証処理に失敗しました。エラーメッセージ:#{e.message}")
      raise '認証処理に失敗しました'
    end
  end
  module_function :register

  class AuthClient
    class << self

      # companyが作成済みでない時だけ新規作成してcompanyオブジェクトを返す
      # @param [Company] company
      # @param [Hash] company_information
      # @return [Company]
      def company_filter(company, company_information)
        company.present? ? company : create_company(company_information)
      end

      # userが作成済みでない時だけ新規作成してuserオブジェクトを返す
      # @param [Company] company
      # @param [Hash] user_information
      # @return [User]
      def user_filter(company, user_information)
        user = User.find_by(uid: user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['uid'])

        if user.present?
          NeApiWrapper.update_token(
            user,
            user_information[NeApiWrapper::PARAMS_RESPONSE_ACCESS_TOKEN],
            user_information[NeApiWrapper::PARAMS_RESPONCE_REFRESH_TOKEN]
          )

          user
        else
          create_user(company.id, user_information)
        end
      end

      # @param [Hash] auth_response
      # @return [Hash] Ne企業情報のHash
      def get_company_information(auth_response)
        NeApiWrapper.api_execute(
          auth_response[NeApiWrapper::PARAMS_RESPONSE_ACCESS_TOKEN],
          auth_response[NeApiWrapper::PARAMS_RESPONCE_REFRESH_TOKEN],
          NeApiWrapper::PATH_API_LOGIN_COMPANY
        )[NeApiWrapper::PARAMS_RESPONSE_DATA][0]
      end

      # @param [Hash] auth_response
      # @return [Hash] Neユーザー情報のHash
      def get_user_information(auth_response)
        NeApiWrapper.api_execute(
          auth_response[NeApiWrapper::PARAMS_RESPONSE_ACCESS_TOKEN],
          auth_response[NeApiWrapper::PARAMS_RESPONCE_REFRESH_TOKEN],
          NeApiWrapper::PATH_API_LOGIN_USER)
      end

      # @param [Hash] company_information
      # @return [Company]
      def create_company(company_information)
        company = Company.new(
          main_function_id_hash: company_information['company_id'],
          ne_company_id_hash:    company_information['company_ne_id'],
          name:                  company_information['company_name'],
          name_kana:             company_information['company_kana']
        )
        unless company.save
          raise '企業情報の登録に失敗しました'
        end

        company
      end

      # @param [Int] company_id
      # @param [Hash] user_information
      # @return [User]
      def create_user(company_id, user_information)
        user = User.new(
          company_id:       company_id,
          uid:              user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['uid'],
          pic_id:           user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['pic_id'],
          pic_ne_id:        user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['pic_ne_id'],
          pic_name:         user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['pic_name'],
          pic_kana:         user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['pic_kana'],
          pic_mail_address: user_information[NeApiWrapper::PARAMS_RESPONSE_DATA][0]['pic_mail_address'],
          user_role_id:     user_information['user_role_id'],
          access_token:     user_information[NeApiWrapper::PARAMS_RESPONSE_ACCESS_TOKEN],
          refresh_token:    user_information[NeApiWrapper::PARAMS_RESPONSE_ACCESS_TOKEN]
        )
        unless user.save
          raise 'ユーザー情報の登録に失敗しました'
        end

        user
      end
    end
  end
end
