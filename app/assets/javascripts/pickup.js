$(document).on('turbolinks:load', function() {
  $('#modal-order').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var order = button.data('order')
    var modal = $(this)

    order = order['order']['info']
    modal.find('.modal-body #goods_location').
      text(order['goods_location'])
    modal.find('.modal-body #receive_order_id').
      text(order['receive_order_id'])
    modal.find('.modal-body #goods_supplier_id').
      text(order['goods_supplier_id'])
    modal.find('.modal-body #goods_foreign_name').
      text(order['goods_foreign_name'])
    modal.find('.modal-body #receive_order_row_goods_id').
      text(order['receive_order_row_goods_id'])
    modal.find('.modal-body #receive_order_row_quantity').
      text(order['receive_order_row_quantity'])
    modal.find('.modal-body #receive_order_consignee_tel').
      text(order['receive_order_consignee_tel'])
    modal.find('.modal-body #receive_order_purchaser_tel').
      text(order['receive_order_purchaser_tel'])
    modal.find('.modal-body #receive_order_consignee_name').
      text(order['receive_order_consignee_name'])
    modal.find('.modal-body #receive_order_purchaser_name').
      text(order['receive_order_purchaser_name'])
    modal.find('.modal-body #receive_order_picking_instruct').
      text(order['receive_order_picking_instruct'])
    modal.find('.modal-body #receive_order_consignee_address1').
      text(order['receive_order_consignee_address1'])
    modal.find('.modal-body #receive_order_consignee_address2').
      text(order['receive_order_consignee_address2'])
    modal.find('.modal-body #receive_order_consignee_zip_code').
      text(order['receive_order_consignee_zip_code'])
    modal.find('.modal-body #receive_order_last_modified_date').
      text(order['receive_order_last_modified_date'])
    modal.find('.modal-body #receive_order_purchaser_address1').
      text(order['receive_order_purchaser_address1'])
    modal.find('.modal-body #receive_order_purchaser_address2').
      text(order['receive_order_purchaser_address2'])
  })

  $('#modal-supplier').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var supplier = button.data('supplier')
    var modal = $(this)

    supplier = supplier['info']

    modal.find('.modal-body #supplier_post_fax').
      text(supplier['supplier_post_fax'])
    modal.find('.modal-body #supplier_id').
      text(supplier['supplier_id'])
    modal.find('.modal-body #supplier_name').
      text(supplier['supplier_name'])
    modal.find('.modal-body #supplier_post_address1').
      text(supplier['supplier_post_address1'])
    modal.find('.modal-body #supplier_post_address2').
      text(supplier['supplier_post_address2'])
    modal.find('.modal-body #supplier_address2').
      text(supplier['supplier_address2'])
    modal.find('.modal-body #supplier_post').
      text(supplier['supplier_post'])
    modal.find('.modal-body #supplier_tel').
      text(supplier['supplier_tel'])
    modal.find('.modal-body #supplier_post_zip_code').
      text(supplier['supplier_post_zip_code'])
    modal.find('.modal-body #supplier_last_modified_date').
      text(supplier['supplier_last_modified_date'])
    modal.find('.modal-body #supplier_mail_address').
      text(supplier['supplier_mail_address'])
  })
});
