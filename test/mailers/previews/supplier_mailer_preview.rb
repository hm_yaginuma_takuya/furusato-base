# Preview all emails at http://localhost:3000/rails/mailers/supplier_mailer
class SupplierMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/supplier_mailer/send
  def call
    SupplierMailer.call(Supplier.first)
  end

end
